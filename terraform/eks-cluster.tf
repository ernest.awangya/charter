# Provider
terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = ">= 5.31.0"
    }
  }
    backend "http" {} 
}

# Configure the AWS Provider
provider "aws" {
  region = "us-east-1"
}

# Resources
# VPC
resource "aws_vpc" "charter-eks--vpc" {
  cidr_block = "10.0.0.0/16"

  tags = {
    Name = "charter-eks-cluster"
  }
}

# Subnets
resource "aws_subnet" "charter-eks--public" {
  cidr_block              = "10.0.1.0/24"
  availability_zone       = "us-east-1a"
  vpc_id                  = aws_vpc.charter-eks--vpc.id
  map_public_ip_on_launch = true

  tags = {
    Name = "charter-eks--public"
  }
}

resource "aws_subnet" "charter-eks--public-2" {
  cidr_block              = "10.0.2.0/24"
  availability_zone       = "us-east-1b"
  vpc_id                  = aws_vpc.charter-eks--vpc.id
  map_public_ip_on_launch = true

  tags = {
    Name = "charter-eks--public-2"
  }
}

# Internet Gateway
resource "aws_internet_gateway" "charter-eks--internet-gateway" {
  vpc_id = aws_vpc.charter-eks--vpc.id

  tags = {
    Name = "charter-eks--internet-gateway"
  }
}

# Route
resource "aws_route" "eks-internet_access" {
  route_table_id         = aws_vpc.charter-eks--vpc.main_route_table_id
  destination_cidr_block = "0.0.0.0/0"
  gateway_id             = aws_internet_gateway.charter-eks--internet-gateway.id
}

# EKS Cluster
resource "aws_eks_cluster" "charter-eks--cluster" {
  name     = "charter-eks--cluster"
  version  = "1.28"
  role_arn = aws_iam_role.charter-eks--cluster-admin-role.arn

  vpc_config {
    subnet_ids              = [aws_subnet.charter-eks--public.id, aws_subnet.charter-eks--public-2.id]
    endpoint_public_access  = true
    endpoint_private_access = true
    public_access_cidrs     = ["0.0.0.0/0"]
  }

  depends_on = [
    aws_iam_role_policy_attachment.charter-eks--cluster-AmazonEKSClusterPolicy, aws_iam_role_policy_attachment.charter-eks--cluster-AmazonEKSVPCResourceController
  ]

  tags = {
    demo = "eks"
  }
}

# IAM Role
data "aws_iam_policy_document" "charter-eks--cluster-admin-role-policy" {
  statement {
    effect = "Allow"
    principals {
      type        = "Service"
      identifiers = ["eks.amazonaws.com"]
    }

    actions = ["sts:AssumeRole"]
  }
}

resource "aws_iam_role" "charter-eks--cluster-admin-role" {
  name               = "charter-eks--cluster-admin-role"
  assume_role_policy = data.aws_iam_policy_document.charter-eks--cluster-admin-role-policy.json
}

resource "aws_iam_role_policy_attachment" "charter-eks--cluster-AmazonEKSClusterPolicy" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonEKSClusterPolicy"
  role       = aws_iam_role.charter-eks--cluster-admin-role.name
}

resource "aws_iam_role_policy_attachment" "charter-eks--cluster-AmazonEKSVPCResourceController" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonEKSVPCResourceController"
  role       = aws_iam_role.charter-eks--cluster-admin-role.name
}

# Addons
resource "aws_eks_addon" "charter-eks--addon-coredns" {
  cluster_name                = aws_eks_cluster.charter-eks--cluster.name
  addon_name                  = "coredns"
  addon_version               = "v1.10.1-eksbuild.2"
  resolve_conflicts_on_create = "OVERWRITE"
}

resource "aws_eks_addon" "charter-eks--addon-kube-proxy" {
  cluster_name                = aws_eks_cluster.charter-eks--cluster.name
  addon_name                  = "kube-proxy"
  addon_version               = "v1.28.1-eksbuild.1"
  resolve_conflicts_on_create = "OVERWRITE"
}

resource "aws_eks_addon" "charter-eks--addon-vpc-cni" {
  cluster_name                = aws_eks_cluster.charter-eks--cluster.name
  addon_name                  = "vpc-cni"
  addon_version               = "v1.14.1-eksbuild.1"
  resolve_conflicts_on_create = "OVERWRITE"
}

# Outputs
output "endpoint" {
  value = aws_eks_cluster.charter-eks--cluster.endpoint
}

output "kubeconfig-certificate-authority-data" {
  value = aws_eks_cluster.charter-eks--cluster.certificate_authority.0.data
}

# Node Group
resource "aws_eks_node_group" "charter-eks--ec2-node-group" {
  cluster_name    = aws_eks_cluster.charter-eks--cluster.name
  node_group_name = "charter-eks--node-group"
  node_role_arn   = aws_iam_role.charter-eks--ec2-node-group-role.arn
  subnet_ids      = [aws_subnet.charter-eks--public.id, aws_subnet.charter-eks--public-2.id]
  instance_types  = ["t3.medium"]

  scaling_config {
    desired_size = 1
    max_size     = 1
    min_size     = 1
  }

  depends_on = [
    aws_iam_role_policy_attachment.charter-eks--node-group-AmazonEKSWorkerNodePolicy,
    aws_iam_role_policy_attachment.charter-eks--node-group-AmazonEKS_CNI_Policy,
    aws_iam_role_policy_attachment.charter-eks--node-group-AmazonEC2ContainerRegistryReadOnly,
  ]

  tags = {
    demo         = "eks"
    eksNodeGroup = "t3_medium"
  }
}

resource "aws_iam_role" "charter-eks--ec2-node-group-role" {
  name = "charter-eks--ec2-node-group-role"
  assume_role_policy = jsonencode({
    Statement = [{
      Action    = "sts:AssumeRole"
      Effect    = "Allow"
      Principal = {
        Service = "ec2.amazonaws.com"
      }
    }]
    Version   = "2012-10-17"
  })
}

resource "aws_iam_role_policy_attachment" "charter-eks--node-group-AmazonEKSWorkerNodePolicy" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonEKSWorkerNodePolicy"
  role       = aws_iam_role.charter-eks--ec2-node-group-role.name
}

resource "aws_iam_role_policy_attachment" "charter-eks--node-group-AmazonEKS_CNI_Policy" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonEKS_CNI_Policy"
  role       = aws_iam_role.charter-eks--ec2-node-group-role.name
}

resource "aws_iam_role_policy_attachment" "charter-eks--node-group-AmazonEC2ContainerRegistryReadOnly" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonEC2ContainerRegistryReadOnly"
  role       = aws_iam_role.charter-eks--ec2-node-group-role.name
}
